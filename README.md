# Millionnaire

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/millionaire.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/millionaire.git
```
