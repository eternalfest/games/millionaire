import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import vault.Vault;
import patchman.IPatch;
import patchman.Patchman;

import millionnaire.Millionnaire;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    debug: Debug,
    millionnaire: Millionnaire,
    merlin: Merlin,
    vault: Vault,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
