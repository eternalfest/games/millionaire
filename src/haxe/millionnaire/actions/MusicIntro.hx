package millionnaire.actions;

import etwin.Obfu;
import merlin.Merlin;
import merlin.IAction;
import merlin.IActionContext;
import patchman.IPatch;
import patchman.Ref;

@:build(patchman.Build.di())
class MusicIntro implements IAction {

  public var name(default, null): String = Obfu.raw("musicIntro");
  public var isVerbose(default, null): Bool = false;

  @:diExport
  public var patch(default, null): IPatch = Ref.auto(hf.mode.GameMode.stopMusic)
    .before(function(hf, self) {
      if (!hf.GameManager.CONFIG.hasMusic())
        return;

      self.manager.musics[self.currentTrack].onSoundComplete = null;
    });
  
  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    var id = ctx.getInt(Obfu.raw("id"));
    var intro = ctx.getInt(Obfu.raw("intro"));

    var game = ctx.getGame();
    game.playMusic(intro);
    var music = game.manager.musics[game.currentTrack];
    music.start();
    music.onSoundComplete = function() {
      game.playMusic(id);
    };

    return false;
  }
}
