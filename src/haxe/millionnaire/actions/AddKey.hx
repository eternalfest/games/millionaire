package millionnaire.actions;

import etwin.Obfu;
import merlin.Merlin;
import merlin.IAction;
import merlin.IActionContext;

class AddKey implements IAction {

  public var name(default, null): String = Obfu.raw("addKey");
  public var isVerbose(default, null): Bool = false;
  
  public function new() {
  }

  public function run(ctx: IActionContext): Bool {
    ctx.getGame().giveKey(ctx.getInt(Obfu.raw("id")));
    return false;
  }
}
