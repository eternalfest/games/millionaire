package millionnaire.items;

import etwin.flash.MovieClip;
import etwin.flash.filters.ColorMatrixFilter;
import hf.Hf;
import hf.SpecialManager;
import hf.entity.Item;
import hf.mode.GameMode;

import color_matrix.ColorMatrix;
import vault.IItem;

class Clairvoyance implements IItem {

  public var id(default, null): Int = 118;
  public var sprite(default, null): Null<String> = null;

  private static inline var JUDGEMENT_ID: Int = 82;
  private static var FILTER: ColorMatrixFilter = ColorMatrix.fromHsv(130, 1.4, 1).toFilter();
  private static var BG_FILTER: ColorMatrixFilter = ColorMatrix.fromHsv(0, 0, 1.1).toFilter();

  public function new() {
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    var game = specMan.game;

    specMan.temporary(this.id, null);
    attachBg(game);
    effectDestroyBad(game);
    specMan.registerRecurring(effectDestroyBad.bind(game), game.root.Data.SECOND, true);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
  }

  private static function attachBg(game: GameMode): Void {
    var view = game.world.view;
    var hf = game.root;

    game.fxMan.fl_bg = true;

    view.attachSpecialBg(hf.Data.BG_PYRAMID, null);
    view._specialBg.filters = [BG_FILTER];
    var extra = view.depthMan.attach("hammer_item_special", hf.Data.DP_SPECIAL_BG);
    extra.gotoAndStop(JUDGEMENT_ID + 1);
    extra.filters = [FILTER];
    extra._x = 200;
    extra._y = 300;
    extra._xscale = 700;
    extra._yscale = extra._xscale;
  }

  private static function effectDestroyBad(game: GameMode): Void {
    var hf = game.root;
    for (bad in game.getList(hf.Data.BAD_CLEAR)) {
      if (!bad.fl_kill) {
        var fx = game.depthMan.attach("hammer_fx_strike", hf.Data.FX);
        fx._x = hf.Data.DOC_WIDTH / 2;
        fx._y = bad._y - hf.Data.CASE_HEIGHT * 0.5;
        fx._xscale *= hf.Std.random(2)*2 - 1;
        fx._yscale = hf.Std.random(50) + 50;


        game.fxMan.attachShine(bad.x, bad.y);
        hf.entity.item.ScoreItem.attach(game, bad.x, bad.y, hf.Data.DIAMANT, null);
        bad.destroy();
        return;
      }
    }
  }

  public function skin(mc: MovieClip, sid: Null<Int>): Void {
    mc.gotoAndStop(JUDGEMENT_ID + 1);
    mc.filters = [FILTER];
  }
}
