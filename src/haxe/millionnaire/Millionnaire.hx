package millionnaire;

import etwin.Obfu;
import etwin.ds.FrozenArray;
import etwin.ds.Nil;
import merlin.IAction;
import merlin.IRefFactory;
import merlin.Merlin;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import vault.IItem;

@:build(patchman.Build.di())
class Millionnaire {

  private static inline var ITEMS_VARIABLE: String = Obfu.raw("ENABLE_LEVEL_ITEMS");

  private static var SPEC_ITEM_SPAWN: IPatch = new PatchList([
    Ref.auto(hf.Data.SPECIAL_ITEM_TIMER).replace(2.5 * 32),
    Ref.auto(hf.StatsManager.spreadExtend).replace(function(hf, self) {}),
    Ref.auto(hf.mode.Adventure.addLevelItems).prefix(function(hf, self) {
      var enable = Merlin.getGlobalVar(self, ITEMS_VARIABLE)
        .map(v => v.unwrapBool())
        .or(false);
      return enable ? Nil.none() : Nil.some(null);
    }),
  ]);

  private static var NO_DARKNESS: IPatch = Ref.auto(hf.Data.MIN_DARKNESS_LEVEL).replace(100);

  @:diExport
  public var playersRef(default, null): IRefFactory = new better_script.players.PlayersRef();

  @:diExport
  public var clairvoyanceItem(default, null): IItem = new millionnaire.items.Clairvoyance();

  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): IPatch;

  public function new(
    musicIntroAction: millionnaire.actions.MusicIntro,
    patches: Array<IPatch>
  ) {
    this.actions = FrozenArray.of(
      new millionnaire.actions.AddKey(),
      musicIntroAction
    );
    
    patches.push(SPEC_ITEM_SPAWN);
    patches.push(NO_DARKNESS);
    this.patches = new PatchList(patches);
  }
}
